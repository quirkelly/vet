<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class HomePageController extends Controller
{
    public function index()
    {
        $bg = '#fff';
	    $response = '';
        try {
            $client = new Client();
            $guzzleResult = $client->request('GET', 'https://go.trustvet.com/api/test');
            $data = $guzzleResult->getBody()->getContents();
            $arr = json_decode($data);
            $bg = $arr->colour;
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $guzzleResult = $e->getResponse();
        }

	    if ($guzzleResult->getStatusCode()!=500) {
		    return view('welcome', compact('bg'));
	    } else {
		    $bg = '#fff';
		    return view('welcome', compact('bg'));	
	    }
	

    }
}
